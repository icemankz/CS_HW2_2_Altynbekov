﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_2_Altynbekov
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shooter> shooters = new List <Shooter>()
        {           
            new Newbie("Петя", 1998, 2), 
            new Skilled("Болат", 1982, 7),
            new Veteran("Иван", 1958, 20),
            new Skilled("Айгуль", 1990, 8),
            new Newbie("Федот", 2001, 1),
            new Skilled("Талгат", 1986, 10),
            new Veteran("Магрипа", 1958, 40),
        };
            foreach (var shooter in shooters)
            {
                shooter.Info();
            }
        }
    }
}
