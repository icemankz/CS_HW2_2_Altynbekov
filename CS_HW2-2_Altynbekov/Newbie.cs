﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_2_Altynbekov
{
    public class Newbie : Shooter
    {
        public Newbie(string name,
            int age,
            int experience)
        : base(name, age, experience)
           {
       
           }

        public override bool Fire() 
        {
            Random rand = new Random();
            double temp;
            temp = Convert.ToDouble(rand.Next(100)) / 100;
            return 0.01 * Experience > (temp);
        }
    }
}
