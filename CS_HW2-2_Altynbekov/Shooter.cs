﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_2_Altynbekov
{
    public abstract class Shooter
    {
        public string Name;
        public int Age;
        public int Experience;

#region Constructors
        public Shooter()
        {

        }

        public Shooter(string name,
            int age,
            int experience)
        {
            Name = name;
            Age = age;
            Experience = experience;
        }
#endregion

        public void Info()
        {
            Console.WriteLine(Name + ", возраст: - " + Age + ", стаж обучения: " + Experience);
            ShowFire();
        }

        public virtual bool Fire()
        {
            Random rand = new Random();
            double temp;
            temp = Convert.ToDouble(rand.Next(100)) / 100;
            return Experience > (temp);
        }
 
        int ShowFire() 
        {
        if (Fire())  
             {
                 Console.WriteLine("Попал!");
                 return 1;
             } 
        else 
             {
                 Console.WriteLine("Не попал!");
                 return 0;
             }
        }
    }
}


